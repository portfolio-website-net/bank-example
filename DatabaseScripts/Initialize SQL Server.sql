CREATE DATABASE BankExample;
GO

USE [BankExample]
GO

CREATE SCHEMA [BankExample];
GO

CREATE LOGIN BankExample WITH PASSWORD = 'BankExample#1';
GO

CREATE USER [BankExample] FOR LOGIN [BankExample]
    WITH DEFAULT_SCHEMA = [BankExample];
GO

ALTER ROLE [db_owner] ADD MEMBER [BankExample];
GO


CREATE TABLE [BankExample].[Users] (
    [UserId]       INT            IDENTITY (1, 1) NOT NULL,
    [Username]     NVARCHAR (50)  NULL,
    [FirstName]    NVARCHAR (100) NULL,
    [LastName]     NVARCHAR (100) NULL,
    [PasswordHash] NVARCHAR (100) NOT NULL,
    [CreatedById]  INT            NOT NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [UpdatedById]  INT            NULL,
    [UpdatedDate]  DATETIME       NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserId] ASC)
);
GO

CREATE TABLE [BankExample].[States] (
    [StateId]     INT           IDENTITY (1, 1) NOT NULL,
    [Code]        NVARCHAR (2)  NULL,
    [Name]        NVARCHAR (50) NULL,
    [CreatedById] INT           NOT NULL,
    [CreatedDate] DATETIME      NOT NULL,
    [UpdatedById] INT           NULL,
    [UpdatedDate] DATETIME      NULL,
    CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED ([StateId] ASC)
);
GO

CREATE TABLE [BankExample].[Profiles] (
    [ProfileId]      INT            IDENTITY (1, 1) NOT NULL,
    [UserId]         INT            NOT NULL,
    [MailingStreet1] NVARCHAR (100) NOT NULL,
    [MailingStreet2] NVARCHAR (100) NULL,
    [MailingCity]    NVARCHAR (100) NOT NULL,
    [MailingStateId] INT            NOT NULL,
    [MailingZipCode] NVARCHAR (10)  NOT NULL,
    [HomePhone]      NVARCHAR (15)  NULL,
    [MobilePhone]    NVARCHAR (15)  NULL,
    [EmailAddress]   NVARCHAR (100) NOT NULL,
    [CreatedById]    INT            NOT NULL,
    [CreatedDate]    DATETIME       NOT NULL,
    [UpdatedById]    INT            NULL,
    [UpdatedDate]    DATETIME       NULL,
    CONSTRAINT [PK_Profiles] PRIMARY KEY CLUSTERED ([ProfileId] ASC),
    CONSTRAINT [FK_Profiles_States] FOREIGN KEY ([MailingStateId]) REFERENCES [BankExample].[States] ([StateId]),
    CONSTRAINT [FK_Profiles_Users] FOREIGN KEY ([UserId]) REFERENCES [BankExample].[Users] ([UserId])
);
GO

CREATE TABLE [BankExample].[AccountTypes] (
    [AccountTypeId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (50) NOT NULL,
    [Code]          NVARCHAR (20) NOT NULL,
    [CreatedById]   INT           NOT NULL,
    [CreatedDate]   DATETIME      NOT NULL,
    [UpdatedById]   INT           NULL,
    [UpdatedDate]   DATETIME      NULL,
    CONSTRAINT [PK_AccountTypes] PRIMARY KEY CLUSTERED ([AccountTypeId] ASC)
);
GO

CREATE TABLE [BankExample].[Accounts] (
    [AccountId]        INT             IDENTITY (1, 1) NOT NULL,
    [AccountTypeId]    INT             NOT NULL,
    [UserId]           INT             NOT NULL,
    [Description]      NVARCHAR (100)  NOT NULL,
    [Balance]          DECIMAL (19, 2) NOT NULL,
    [LastActivityDate] DATETIME        NOT NULL,
    [PaymentDueDate]   DATE            NULL,
    [MinimumAmountDue] DECIMAL (19, 2) NULL,
    [InterestRate]     FLOAT (53)      NULL,
    [MaturityDate]     DATE            NULL,
    [CreatedById]      INT             NOT NULL,
    [CreatedDate]      DATETIME        NOT NULL,
    [UpdatedById]      INT             NULL,
    [UpdatedDate]      DATETIME        NULL,
    CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED ([AccountId] ASC),
    CONSTRAINT [FK_Accounts_AccountTypes] FOREIGN KEY ([AccountTypeId]) REFERENCES [BankExample].[AccountTypes] ([AccountTypeId]),
    CONSTRAINT [FK_Accounts_Users] FOREIGN KEY ([UserId]) REFERENCES [BankExample].[Users] ([UserId])
);
GO


-- Populate Users
INSERT INTO bankexample.Users ([Username], [FirstName], [LastName], [PasswordHash], [CreatedById], [CreatedDate]) 
VALUES ('bsmith', 'Bill', 'Smith', '', 0, GetDate()),
('hwilson', 'Hannah', 'Wilson', '', 0, GetDate()),
('mjackson', 'Mary', 'Jackson', '', 0, GetDate());

-- Populate AccountTypes
INSERT INTO bankexample.AccountTypes ([Name], [Code], [CreatedById], [CreatedDate])
VALUES ('Savings Account', 'Savings', 0, GetDate()),
('Checking Account', 'Checking', 0, GetDate()),
('Loan Account', 'Loan', 0, GetDate()),
('Certificate of Deposit Account', 'Certificate', 0, GetDate());

-- Populate Accounts
INSERT INTO bankexample.Accounts ([UserId], [AccountTypeId], [Description], [Balance], [LastActivityDate], [PaymentDueDate], [MinimumAmountDue], [InterestRate], [MaturityDate], [CreatedById], [CreatedDate]) 
VALUES ((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Savings'), 'Bill''s Savings', 12000, GetDate(), NULL, NULL, NULL, NULL, 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Savings'),'Hannah''s Savings', 7000, GetDate(), NULL, NULL, NULL, NULL, 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Savings'),'Mary''s Savings', 19000, GetDate(), NULL, NULL, NULL, NULL, 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Checking'), 'Bill''s Checking', 2143.22, GetDate(),  NULL, NULL, NULL, NULL, 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Checking'), 'Mary''s Checking', 3500, GetDate(),  NULL, NULL, NULL, NULL, 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Loan'), 'Bill''s Car Loan', 6700, GetDate(), '2018-12-01', 200, NULL, NULL, 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Loan'), 'Hannah''s Home Loan', 122000, GetDate(), '2018-12-01', 800, NULL, NULL, 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Loan'), 'Hannah''s Student Loan', 4500, GetDate(), '2018-11-15', 100, NULL, NULL, 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Loan'), 'Hannah''s Car Loan', 8845.12, GetDate(), '2018-11-01', 200, NULL, NULL, 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Certificate'), 'Bill''s 3 Year CD', 5000, GetDate(), NULL, NULL, 0.02, '2019-01-01', 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Certificate'), 'Hannah''s 1 Year CD', 2500, GetDate(), NULL, NULL, 0.014, '2018-12-01', 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Certificate'), 'Mary''s 2 Year CD', 5000, GetDate(), NULL, NULL, 0.015, '2019-06-01', 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Certificate'), 'Mary''s 5 Year CD', 6000, GetDate(), NULL, NULL, 0.03, '2022-01-01', 0, GetDate());

-- Populate US States
INSERT INTO bankexample.States ([Code], [Name], [CreatedById], [CreatedDate]) 
VALUES ('AL', 'Alabama', 0, GetDate()),
('AK', 'Alaska', 0, GetDate()),
('AZ', 'Arizona', 0, GetDate()),
('AR', 'Arkansas', 0, GetDate()),
('CA', 'California', 0, GetDate()),
('CO', 'Colorado', 0, GetDate()),
('CT', 'Connecticut', 0, GetDate()),
('DE', 'Delaware', 0, GetDate()),
('DC', 'District of Columbia', 0, GetDate()),
('FL', 'Florida', 0, GetDate()),
('GA', 'Georgia', 0, GetDate()),
('HI', 'Hawaii', 0, GetDate()),
('ID', 'Idaho', 0, GetDate()),
('IL', 'Illinois', 0, GetDate()),
('IN', 'Indiana', 0, GetDate()),
('IA', 'Iowa', 0, GetDate()),
('KS', 'Kansas', 0, GetDate()),
('KY', 'Kentucky', 0, GetDate()),
('LA', 'Louisiana', 0, GetDate()),
('ME', 'Maine', 0, GetDate()),
('MD', 'Maryland', 0, GetDate()),
('MA', 'Massachusetts', 0, GetDate()),
('MI', 'Michigan', 0, GetDate()),
('MN', 'Minnesota', 0, GetDate()),
('MS', 'Mississippi', 0, GetDate()),
('MO', 'Missouri', 0, GetDate()),
('MT', 'Montana', 0, GetDate()),
('NE', 'Nebraska', 0, GetDate()),
('NV', 'Nevada', 0, GetDate()),
('NH', 'New Hampshire', 0, GetDate()),
('NJ', 'New Jersey', 0, GetDate()),
('NM', 'New Mexico', 0, GetDate()),
('NY', 'New York', 0, GetDate()),
('NC', 'North Carolina', 0, GetDate()),
('ND', 'North Dakota', 0, GetDate()),
('OH', 'Ohio', 0, GetDate()),
('OK', 'Oklahoma', 0, GetDate()),
('OR', 'Oregon', 0, GetDate()),
('PA', 'Pennsylvania', 0, GetDate()),
('PR', 'Puerto Rico', 0, GetDate()),
('RI', 'Rhode Island', 0, GetDate()),
('SC', 'South Carolina', 0, GetDate()),
('SD', 'South Dakota', 0, GetDate()),
('TN', 'Tennessee', 0, GetDate()),
('TX', 'Texas', 0, GetDate()),
('UT', 'Utah', 0, GetDate()),
('VT', 'Vermont', 0, GetDate()),
('VA', 'Virginia', 0, GetDate()),
('WA', 'Washington', 0, GetDate()),
('WV', 'West Virginia', 0, GetDate()),
('WI', 'Wisconsin', 0, GetDate()),
('WY', 'Wyoming', 0, GetDate());

-- Populate Profiles
INSERT INTO bankexample.Profiles ([UserId], [MailingStreet1], [MailingStreet2], [MailingCity], [MailingStateId], [MailingZipCode], [HomePhone], [MobilePhone], [EmailAddress], [CreatedById], [CreatedDate]) 
VALUES ((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), '100 Main St.', NULL, 'Harrisburg', (SELECT StateId FROM bankexample.States WHERE Code = 'PA'), '17110', '717-123-1234', NULL, 'bsmith@gmail.com', 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), '200 West Ave.', NULL, 'Mechanicsburg', (SELECT StateId FROM bankexample.States WHERE Code = 'PA'), '17055', '717-213-2333', '717-609-1221', 'hwilson@gmail.com', 0, GetDate()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), '300 East St.', NULL, 'Harrisburg', (SELECT StateId FROM bankexample.States WHERE Code = 'PA'), '17110', '717-653-2311', '717-609-3511', 'mjackson@gmail.com', 0, GetDate());

