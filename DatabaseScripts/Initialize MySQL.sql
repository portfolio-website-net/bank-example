CREATE SCHEMA `bankexample`;

CREATE TABLE `BankExample`.Users (
    `UserId`       INT            AUTO_INCREMENT  NOT NULL,
    `Username`     NVARCHAR (50)  NULL,
    `FirstName`    NVARCHAR (100) NULL,
    `LastName`     NVARCHAR (100) NULL,
    `PasswordHash` NVARCHAR (100) NOT NULL,
    `CreatedById`  INT            NOT NULL,
    `CreatedDate`  DATETIME(3)       NOT NULL,
    `UpdatedById`  INT            NULL,
    `UpdatedDate`  DATETIME(3)       NULL,
    CONSTRAINT `PK_Users` PRIMARY KEY (`UserId` ASC)
);

CREATE TABLE `BankExample`.States (
    `StateId`     INT           AUTO_INCREMENT  NOT NULL,
    `Code`        NVARCHAR (2)  NULL,
    `Name`        NVARCHAR (50) NULL,
    `CreatedById` INT           NOT NULL,
    `CreatedDate` DATETIME(3)      NOT NULL,
    `UpdatedById` INT           NULL,
    `UpdatedDate` DATETIME(3)      NULL,
    CONSTRAINT `PK_States` PRIMARY KEY (`StateId` ASC)
);

CREATE TABLE `BankExample`.Profiles (
    `ProfileId`      INT            AUTO_INCREMENT  NOT NULL,
    `UserId`         INT            NOT NULL,
    `MailingStreet1` NVARCHAR (100) NOT NULL,
    `MailingStreet2` NVARCHAR (100) NULL,
    `MailingCity`    NVARCHAR (100) NOT NULL,
    `MailingStateId` INT            NOT NULL,
    `MailingZipCode` NVARCHAR (10)  NOT NULL,
    `HomePhone`      NVARCHAR (15)  NULL,
    `MobilePhone`    NVARCHAR (15)  NULL,
    `EmailAddress`   NVARCHAR (100) NOT NULL,
    `CreatedById`    INT            NOT NULL,
    `CreatedDate`    DATETIME(3)       NOT NULL,
    `UpdatedById`    INT            NULL,
    `UpdatedDate`    DATETIME(3)       NULL,
    CONSTRAINT `PK_Profiles` PRIMARY KEY (`ProfileId` ASC),
    CONSTRAINT `FK_Profiles_States` FOREIGN KEY (`MailingStateId`) REFERENCES `BankExample`.States (`StateId`),
    CONSTRAINT `FK_Profiles_Users` FOREIGN KEY (`UserId`) REFERENCES `BankExample`.Users (`UserId`)
);

CREATE TABLE `BankExample`.AccountTypes (
    `AccountTypeId` INT           AUTO_INCREMENT  NOT NULL,
    `Name`          NVARCHAR (50) NOT NULL,
    `Code`          NVARCHAR (20) NOT NULL,
    `CreatedById`   INT           NOT NULL,
    `CreatedDate`   DATETIME(3)      NOT NULL,
    `UpdatedById`   INT           NULL,
    `UpdatedDate`   DATETIME(3)      NULL,
    CONSTRAINT `PK_AccountTypes` PRIMARY KEY (`AccountTypeId` ASC)
);

CREATE TABLE `BankExample`.Accounts (
    `AccountId`        INT             AUTO_INCREMENT  NOT NULL,
    `AccountTypeId`    INT             NOT NULL,
    `UserId`           INT             NOT NULL,
    `Description`      NVARCHAR (100)  NOT NULL,
    `Balance`          DECIMAL (19, 2) NOT NULL,
    `LastActivityDate` DATETIME(3)        NOT NULL,
    `PaymentDueDate`   DATE            NULL,
    `MinimumAmountDue` DECIMAL (19, 2) NULL,
    `InterestRate`     DOUBLE       NULL,
    `MaturityDate`     DATE            NULL,
    `CreatedById`      INT             NOT NULL,
    `CreatedDate`      DATETIME(3)        NOT NULL,
    `UpdatedById`      INT             NULL,
    `UpdatedDate`      DATETIME(3)        NULL,
    CONSTRAINT `PK_Accounts` PRIMARY KEY (`AccountId` ASC),
    CONSTRAINT `FK_Accounts_AccountTypes` FOREIGN KEY (`AccountTypeId`) REFERENCES `BankExample`.AccountTypes (`AccountTypeId`),
    CONSTRAINT `FK_Accounts_Users` FOREIGN KEY (`UserId`) REFERENCES `BankExample`.Users (`UserId`)
);


-- Populate Users
INSERT INTO bankexample.Users (`Username`, `FirstName`, `LastName`, `PasswordHash`, `CreatedById`, `CreatedDate`) 
VALUES ('bsmith', 'Bill', 'Smith', '', 0, NOW()),
('hwilson', 'Hannah', 'Wilson', '', 0, NOW()),
('mjackson', 'Mary', 'Jackson', '', 0, NOW());

-- Populate AccountTypes
INSERT INTO bankexample.AccountTypes (`Name`, `Code`, `CreatedById`, `CreatedDate`)
VALUES ('Savings Account', 'Savings', 0, NOW()),
('Checking Account', 'Checking', 0, NOW()),
('Loan Account', 'Loan', 0, NOW()),
('Certificate of Deposit Account', 'Certificate', 0, NOW());

-- Populate Accounts
INSERT INTO bankexample.Accounts (`UserId`, `AccountTypeId`, `Description`, `Balance`, `LastActivityDate`, `PaymentDueDate`, `MinimumAmountDue`, `InterestRate`, `MaturityDate`, `CreatedById`, `CreatedDate`) 
VALUES ((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Savings'), 'Bill''s Savings', 12000, NOW(), NULL, NULL, NULL, NULL, 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Savings'),'Hannah''s Savings', 7000, NOW(), NULL, NULL, NULL, NULL, 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Savings'),'Mary''s Savings', 19000, NOW(), NULL, NULL, NULL, NULL, 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Checking'), 'Bill''s Checking', 2143.22, NOW(),  NULL, NULL, NULL, NULL, 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Checking'), 'Mary''s Checking', 3500, NOW(),  NULL, NULL, NULL, NULL, 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Loan'), 'Bill''s Car Loan', 6700, NOW(), '2018-12-01', 200, NULL, NULL, 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Loan'), 'Hannah''s Home Loan', 122000, NOW(), '2018-12-01', 800, NULL, NULL, 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Loan'), 'Hannah''s Student Loan', 4500, NOW(), '2018-11-15', 100, NULL, NULL, 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Loan'), 'Hannah''s Car Loan', 8845.12, NOW(), '2018-11-01', 200, NULL, NULL, 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Certificate'), 'Bill''s 3 Year CD', 5000, NOW(), NULL, NULL, 0.02, '2019-01-01', 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Certificate'), 'Hannah''s 1 Year CD', 2500, NOW(), NULL, NULL, 0.014, '2018-12-01', 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Certificate'), 'Mary''s 2 Year CD', 5000, NOW(), NULL, NULL, 0.015, '2019-06-01', 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), (SELECT AccountTypeId FROM bankexample.AccountTypes WHERE Code = 'Certificate'), 'Mary''s 5 Year CD', 6000, NOW(), NULL, NULL, 0.03, '2022-01-01', 0, NOW());

-- Populate US States
INSERT INTO bankexample.States (`Code`, `Name`, `CreatedById`, `CreatedDate`) 
VALUES ('AL', 'Alabama', 0, NOW()),
('AK', 'Alaska', 0, NOW()),
('AZ', 'Arizona', 0, NOW()),
('AR', 'Arkansas', 0, NOW()),
('CA', 'California', 0, NOW()),
('CO', 'Colorado', 0, NOW()),
('CT', 'Connecticut', 0, NOW()),
('DE', 'Delaware', 0, NOW()),
('DC', 'District of Columbia', 0, NOW()),
('FL', 'Florida', 0, NOW()),
('GA', 'Georgia', 0, NOW()),
('HI', 'Hawaii', 0, NOW()),
('ID', 'Idaho', 0, NOW()),
('IL', 'Illinois', 0, NOW()),
('IN', 'Indiana', 0, NOW()),
('IA', 'Iowa', 0, NOW()),
('KS', 'Kansas', 0, NOW()),
('KY', 'Kentucky', 0, NOW()),
('LA', 'Louisiana', 0, NOW()),
('ME', 'Maine', 0, NOW()),
('MD', 'Maryland', 0, NOW()),
('MA', 'Massachusetts', 0, NOW()),
('MI', 'Michigan', 0, NOW()),
('MN', 'Minnesota', 0, NOW()),
('MS', 'Mississippi', 0, NOW()),
('MO', 'Missouri', 0, NOW()),
('MT', 'Montana', 0, NOW()),
('NE', 'Nebraska', 0, NOW()),
('NV', 'Nevada', 0, NOW()),
('NH', 'New Hampshire', 0, NOW()),
('NJ', 'New Jersey', 0, NOW()),
('NM', 'New Mexico', 0, NOW()),
('NY', 'New York', 0, NOW()),
('NC', 'North Carolina', 0, NOW()),
('ND', 'North Dakota', 0, NOW()),
('OH', 'Ohio', 0, NOW()),
('OK', 'Oklahoma', 0, NOW()),
('OR', 'Oregon', 0, NOW()),
('PA', 'Pennsylvania', 0, NOW()),
('PR', 'Puerto Rico', 0, NOW()),
('RI', 'Rhode Island', 0, NOW()),
('SC', 'South Carolina', 0, NOW()),
('SD', 'South Dakota', 0, NOW()),
('TN', 'Tennessee', 0, NOW()),
('TX', 'Texas', 0, NOW()),
('UT', 'Utah', 0, NOW()),
('VT', 'Vermont', 0, NOW()),
('VA', 'Virginia', 0, NOW()),
('WA', 'Washington', 0, NOW()),
('WV', 'West Virginia', 0, NOW()),
('WI', 'Wisconsin', 0, NOW()),
('WY', 'Wyoming', 0, NOW());

-- Populate Profiles
INSERT INTO bankexample.Profiles (`UserId`, `MailingStreet1`, `MailingStreet2`, `MailingCity`, `MailingStateId`, `MailingZipCode`, `HomePhone`, `MobilePhone`, `EmailAddress`, `CreatedById`, `CreatedDate`) 
VALUES ((SELECT UserId FROM bankexample.Users WHERE Username = 'bsmith'), '100 Main St.', NULL, 'Harrisburg', (SELECT StateId FROM bankexample.States WHERE Code = 'PA'), '17110', '717-123-1234', NULL, 'bsmith@gmail.com', 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'hwilson'), '200 West Ave.', NULL, 'Mechanicsburg', (SELECT StateId FROM bankexample.States WHERE Code = 'PA'), '17055', '717-213-2333', '717-609-1221', 'hwilson@gmail.com', 0, NOW()),
((SELECT UserId FROM bankexample.Users WHERE Username = 'mjackson'), '300 East St.', NULL, 'Harrisburg', (SELECT StateId FROM bankexample.States WHERE Code = 'PA'), '17110', '717-653-2311', '717-609-3511', 'mjackson@gmail.com', 0, NOW());

