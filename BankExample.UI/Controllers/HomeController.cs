﻿// <copyright file="HomeController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.UI.Controllers
{
    using System.Web.Configuration;
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            this.ViewBag.BankAPIUrl = WebConfigurationManager.AppSettings["BankAPIUrl"];

            return this.View();
        }
    }
}