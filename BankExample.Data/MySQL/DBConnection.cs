﻿// <copyright file="DBConnection.cs" company="BankExample">
// Copyright (c) BankExample. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Data.MySQL
{
    using System.Configuration;
    using MySql.Data.MySqlClient;

    // Reference URL: https://stackoverflow.com/questions/21618015/how-to-connect-to-mysql-database
    public class DBConnection
    {
        private static DBConnection instance = null;
        private MySqlConnection connection = null;

        private DBConnection()
        {
        }

        public MySqlConnection Connection
        {
            get
            {
                return this.connection;
            }
        }

        public static DBConnection Instance()
        {
            instance = new DBConnection();
            return instance;
        }

        public bool IsConnect()
        {
            if (this.Connection == null)
            {
                string connectionString = string.Format(ConfigurationManager.ConnectionStrings["MySQLDefaultConnection"].ConnectionString);
                this.connection = new MySqlConnection(connectionString);
                this.connection.Open();
            }

            return true;
        }

        public void Close()
        {
            this.connection.Close();
        }
    }
}
