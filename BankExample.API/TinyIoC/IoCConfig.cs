﻿// <copyright file="IoCConfig.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.API.TinyIoC
{
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Interfaces;
    using Business.Providers;
    using global::TinyIoC;

    public static class IoCConfig
    {
        public static void Register()
        {
            var container = TinyIoCContainer.Current;

            if (WebConfigurationManager.AppSettings["DatabaseType"] == "MySQL")
            {
                container.Register<IBankAccounts, MySQLBankAccounts>().AsPerRequestSingleton();
                container.Register<IProfiles, MySQLProfiles>().AsPerRequestSingleton();
                container.Register<ISecurity, MySQLSecurity>().AsPerRequestSingleton();
            }
            else
            {
                container.Register<IBankAccounts, SQLServerBankAccounts>().AsPerRequestSingleton();
                container.Register<IProfiles, SQLServerProfiles>().AsPerRequestSingleton();
                container.Register<ISecurity, SQLServerSecurity>().AsPerRequestSingleton();
            }

            DependencyResolver.SetResolver(new TinyIocMvcDependencyResolver(container));
        }
    }
}