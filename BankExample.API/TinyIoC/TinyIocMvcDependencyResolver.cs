﻿// <copyright file="TinyIocMvcDependencyResolver.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.API.TinyIoC
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using global::TinyIoC;

    // Source URL: http://blog.i-m-code.com/2014/04/15/tinyioc-mvc-and-webapi-configuration/
    public class TinyIocMvcDependencyResolver : IDependencyResolver
    {
        private TinyIoCContainer container;

        public TinyIocMvcDependencyResolver(TinyIoCContainer container)
        {
            this.container = container;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return this.container.Resolve(serviceType);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return this.container.ResolveAll(serviceType, true);
            }
            catch (Exception)
            {
                return Enumerable.Empty<object>();
            }
        }
    }
}