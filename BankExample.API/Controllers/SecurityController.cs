﻿// <copyright file="SecurityController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.API.Controllers
{
    using System.Web.Http;
    using System.Web.Http.Cors;
    using Base;
    using Business.Interfaces;
    using Business.ViewModels;
    using RazHeaderAttribute.Attributes;

    // Allow CORS for all origins. (Caution!)
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SecurityController : BaseApiController
    {
        private ISecurity securityProvider;
        private IProfiles profilesProvider;

        public SecurityController()
        {
            this.securityProvider = this.GetSecurityProvider();
            this.profilesProvider = this.GetProfilesProvider();
        }

        [HttpGet]
        public ProfileInfoViewModel Get([FromHeader("username")] string username, [FromHeader("password")] string password)
        {
            // Initialize passwords for new user accounts (only needed for this example to populate the database)
            this.securityProvider.InitializeUserPasswordHashes();

            var securityTokenViewModel = this.securityProvider.GetSecurityToken(username, password);

            if (!string.IsNullOrEmpty(securityTokenViewModel.SecurityToken))
            {
                return this.profilesProvider.Hydrate(securityTokenViewModel.UserId);
            }
            else
            {
                return new ProfileInfoViewModel();
            }
        }
    }
}
