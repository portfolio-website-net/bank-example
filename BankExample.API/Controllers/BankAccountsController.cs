﻿// <copyright file="BankAccountsController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.API.Controllers
{
    using System.Collections.Generic;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using Base;
    using Business.Interfaces;
    using Business.ViewModels;
    using RazHeaderAttribute.Attributes;

    // Allow CORS for all origins. (Caution!)
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BankAccountsController : BaseApiController
    {
        private ISecurity securityProvider;
        private IBankAccounts bankAccountProvider;

        public BankAccountsController()
        {
            this.securityProvider = this.GetSecurityProvider();
            this.bankAccountProvider = this.GetBankAccountsProvider();
        }

        [HttpGet]
        public List<BankAccountViewModel> Get([FromHeader("securityToken")] string securityToken, int id)
        {
            if (this.securityProvider.ValidateUserLogin(id, securityToken))
            {
                return this.bankAccountProvider.Hydrate(id);
            }
            else
            {
                return new List<BankAccountViewModel>();
            }
        }
    }
}
