﻿// <copyright file="BaseApiController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.API.Controllers.Base
{
    using System;
    using System.Web.Configuration;
    using System.Web.Http;
    using Business.Helpers;
    using Business.Interfaces;
    using Business.Providers;
    using global::TinyIoC;

    public class BaseApiController : ApiController
    {
        protected ISecurity GetSecurityProvider()
        {
            if (this.GetDatabaseType() == DatabaseTypes.MySQL)
            {
                return TinyIoCContainer.Current.Resolve<MySQLSecurity>();
            }

            return TinyIoCContainer.Current.Resolve<SQLServerSecurity>();
        }

        protected IProfiles GetProfilesProvider()
        {
            if (this.GetDatabaseType() == DatabaseTypes.MySQL)
            {
                return TinyIoCContainer.Current.Resolve<MySQLProfiles>();
            }

            return TinyIoCContainer.Current.Resolve<SQLServerProfiles>();
        }

        protected IBankAccounts GetBankAccountsProvider()
        {
            if (this.GetDatabaseType() == DatabaseTypes.MySQL)
            {
                return TinyIoCContainer.Current.Resolve<MySQLBankAccounts>();
            }

            return TinyIoCContainer.Current.Resolve<SQLServerBankAccounts>();
        }

        private DatabaseTypes GetDatabaseType()
        {
            string databaseType = WebConfigurationManager.AppSettings["DatabaseType"];

            DatabaseTypes databaseTypeVal;

            if (string.IsNullOrEmpty(databaseType)
                || !Enum.TryParse(databaseType, out databaseTypeVal))
            {
                databaseTypeVal = DatabaseTypes.SQLServer;
            }

            return databaseTypeVal;
        }
    }
}
