﻿// <copyright file="ProfilesController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.API.Controllers
{
    using System.Web.Http;
    using System.Web.Http.Cors;
    using Base;
    using Business.Interfaces;
    using Business.ViewModels;
    using RazHeaderAttribute.Attributes;

    // Allow CORS for all origins. (Caution!)
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProfilesController : BaseApiController
    {
        private ISecurity securityProvider;
        private IProfiles profilesProvider;

        public ProfilesController()
        {
            this.securityProvider = this.GetSecurityProvider();
            this.profilesProvider = this.GetProfilesProvider();
        }

        [HttpGet]
        public ProfileInfoViewModel Get([FromHeader("securityToken")] string securityToken, int id)
        {
            if (this.securityProvider.ValidateUserLogin(id, securityToken))
            {
                return this.profilesProvider.Hydrate(id);
            }
            else
            {
                return new ProfileInfoViewModel();
            }
        }

        [HttpPost]
        public ProfileInfoViewModel Post([FromHeader("securityToken")] string securityToken, UpdatableProfileInfoViewModel viewModel)
        {
            if (this.securityProvider.ValidateUserLogin(viewModel.UserId, securityToken))
            {
                return this.profilesProvider.Dehydrate(viewModel);
            }
            else
            {
                return new ProfileInfoViewModel();
            }
        }
    }
}
