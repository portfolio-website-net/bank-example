﻿// <copyright file="StateViewModel.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.ViewModels
{
    public class StateViewModel
    {
        public int StateId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }
    }
}
