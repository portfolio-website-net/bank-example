﻿// <copyright file="ProfileInfoViewModel.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.ViewModels
{
    using System.Collections.Generic;

    public class ProfileInfoViewModel
    {
        public string SecurityToken { get; set; }

        public int ProfileId { get; set; }

        public int UserId { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FirstName)
                    && !string.IsNullOrEmpty(this.LastName))
                {
                    return this.FirstName + " " + this.LastName;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public MailingAddressViewModel MailingAddress { get; set; }

        public string HomePhone { get; set; }

        public string MobilePhone { get; set; }

        public string EmailAddress { get; set; }

        public List<string> ValidationMessages { get; set; }
    }
}
