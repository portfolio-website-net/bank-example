﻿// <copyright file="BankAccountViewModel.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.ViewModels
{
    using System;

    public class BankAccountViewModel
    {
        public int AccountId { get; set; }

        public int AccountTypeId { get; set; }

        public string AccountTypeName { get; set; }

        public string AccountTypeCode { get; set; }

        public int UserId { get; set; }

        public string Username { get; set; }

        public string Description { get; set; }

        public decimal Balance { get; set; }

        public string BalanceString
        {
            get
            {
                return this.Balance.ToString("c");
            }
        }

        public DateTime LastActivityDate { get; set; }

        public string LastActivityDateString
        {
            get
            {
                return this.LastActivityDate.ToString("M/d/yyyy");
            }
        }

        public DateTime? PaymentDueDate { get; set; }

        public string PaymentDueDateString
        {
            get
            {
                if (this.PaymentDueDate.HasValue)
                {
                    return this.PaymentDueDate.Value.ToString("M/d/yyyy");
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public decimal? MinimumAmountDue { get; set; }

        public string MinimumAmountDueString
        {
            get
            {
                if (this.MinimumAmountDue.HasValue)
                {
                    return this.MinimumAmountDue.Value.ToString("c");
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public double? InterestRate { get; set; }

        public string InterestRateString
        {
            get
            {
                if (this.InterestRate.HasValue)
                {
                    return this.InterestRate.Value.ToString("0.00%");
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public DateTime? MaturityDate { get; set; }

        public string MaturityDateString
        {
            get
            {
                if (this.MaturityDate.HasValue)
                {
                    return this.MaturityDate.Value.ToString("M/d/yyyy");
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
}
