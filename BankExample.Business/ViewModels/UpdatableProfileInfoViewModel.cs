﻿// <copyright file="UpdatableProfileInfoViewModel.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.ViewModels
{
    public class UpdatableProfileInfoViewModel
    {
        public string SecurityToken { get; set; }

        public int ProfileId { get; set; }

        public int UserId { get; set; }

        public UpdatableMailingAddressViewModel MailingAddress { get; set; }

        public string HomePhone { get; set; }

        public string MobilePhone { get; set; }

        public string EmailAddress { get; set; }
    }
}
