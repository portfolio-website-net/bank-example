﻿// <copyright file="ISecurity.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Interfaces
{
    using ViewModels;

    public interface ISecurity
    {
        SecurityTokenViewModel GetSecurityToken(string username, string password);

        bool ValidateUserLogin(int userId, string securityToken);

        void InitializeUserPasswordHashes();
    }
}
