﻿// <copyright file="IProfiles.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Interfaces
{
    using ViewModels;

    public interface IProfiles
    {
        ProfileInfoViewModel Hydrate(int userId);

        ProfileInfoViewModel Dehydrate(UpdatableProfileInfoViewModel viewModel);
    }
}
