﻿// <copyright file="MySQLProfiles.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Providers
{
    using System.Collections.Generic;
    using System.Linq;
    using Data.MySQL;
    using Helpers;
    using Interfaces;
    using MySql.Data.MySqlClient;
    using ViewModels;

    public class MySQLProfiles : IProfiles
    {
        public ProfileInfoViewModel Hydrate(int userId)
        {
            var viewModel = new ProfileInfoViewModel();

            var dbConnection = DBConnection.Instance();
            if (dbConnection.IsConnect())
            {
                string query = @"SELECT p.ProfileId, 
                                        p.UserId,
                                        u.Username,
                                        u.PasswordHash,
                                        u.FirstName,
                                        u.LastName,
                                        p.MailingStreet1,
                                        p.MailingStreet2,
                                        p.MailingCity,
                                        p.MailingStateId,
                                        s.Code AS MailingStateCode,
                                        p.MailingZipCode,
                                        p.HomePhone,
                                        p.MobilePhone,
                                        p.EmailAddress
                                   FROM bankexample.Profiles p
                                  INNER JOIN bankexample.States s ON p.MailingStateId = s.StateId
                                  INNER JOIN bankexample.Users u ON p.UserId = u.UserId
                                  WHERE u.UserId = @userId";

                var cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@userId", userId);

                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    viewModel.SecurityToken = Sha256Hash.Sha256(reader.GetString("Username") + reader.GetString("PasswordHash") + Constants.ApplicationSecurityToken);
                    viewModel.ProfileId = reader.GetInt32("ProfileId");
                    viewModel.UserId = reader.GetInt32("UserId");
                    viewModel.Username = reader.GetString("Username");
                    viewModel.FirstName = reader.GetString("FirstName");
                    viewModel.LastName = reader.GetString("LastName");

                    int mailingStreet2Index = reader.GetOrdinal("MailingStreet2");
                    int homePhoneIndex = reader.GetOrdinal("HomePhone");
                    int mobilePhoneIndex = reader.GetOrdinal("MobilePhone");

                    viewModel.MailingAddress = new MailingAddressViewModel
                    {
                        Street1 = reader.GetString("MailingStreet1"),
                        Street2 = reader.IsDBNull(mailingStreet2Index) ? null : reader.GetString("MailingStreet2"),
                        City = reader.GetString("MailingCity"),
                        StateId = reader.GetInt32("MailingStateId"),
                        State = reader.GetString("MailingStateCode"),
                        ZipCode = reader.GetString("MailingZipCode")
                    };

                    viewModel.HomePhone = reader.IsDBNull(homePhoneIndex) ? null : reader.GetString("HomePhone");
                    viewModel.MobilePhone = reader.IsDBNull(mobilePhoneIndex) ? null : reader.GetString("MobilePhone");
                    viewModel.EmailAddress = reader.GetString("EmailAddress");
                }

                reader.Close();

                query = @"SELECT s.StateId, 
                                 s.Name,
                                 s.Code
                            FROM bankexample.States s";

                cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                viewModel.MailingAddress.StatesList = new List<StateViewModel>();

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    viewModel.MailingAddress.StatesList.Add(new StateViewModel
                    {
                        StateId = reader.GetInt32("StateId"),
                        Name = reader.GetString("Name"),
                        Code = reader.GetString("Code"),
                    });
                }

                dbConnection.Close();
            }

            return viewModel;
        }

        public ProfileInfoViewModel Dehydrate(UpdatableProfileInfoViewModel viewModel)
        {
            var result = new ProfileInfoViewModel();

            result.ValidationMessages = ProfileValidation.Validate(viewModel);

            if (!result.ValidationMessages.Any())
            {
                var dbConnection = DBConnection.Instance();
                if (dbConnection.IsConnect())
                {
                    string query = @"SELECT p.ProfileId
                                       FROM bankexample.Profiles p
                                      INNER JOIN bankexample.Users u ON p.UserId = u.UserId
                                      WHERE u.UserId = @userId
                                        AND p.ProfileId = @profileId";

                    var cmd = new MySqlCommand();
                    cmd.Connection = dbConnection.Connection;
                    cmd.CommandText = query;
                    cmd.Prepare();

                    cmd.Parameters.AddWithValue("@userId", viewModel.UserId);
                    cmd.Parameters.AddWithValue("@profileId", viewModel.ProfileId);

                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader.GetInt32("ProfileId") == viewModel.ProfileId)
                        {
                            reader.Close();

                            var mailingAddressStateId = this.GetMailingAddressStateId(viewModel, dbConnection);

                            query = @"UPDATE bankexample.Profiles
                                     SET MailingStreet1 = @mailingStreet1,
                                         MailingStreet2 = @mailingStreet2,
                                         MailingCity = @MailingCity,
                                         MailingStateId = @mailingStateId,
                                         MailingZipCode = @mailingZipCode,
                                         HomePhone = @homePhone,
                                         MobilePhone = @mobilePhone,
                                         EmailAddress = @emailAddress,
                                         UpdatedById = @userId,
                                         UpdatedDate = NOW()
                                   WHERE ProfileId = @profileId";

                            cmd = new MySqlCommand();
                            cmd.Connection = dbConnection.Connection;
                            cmd.CommandText = query;
                            cmd.Prepare();

                            cmd.Parameters.AddWithValue("@mailingStreet1", viewModel.MailingAddress.Street1);
                            cmd.Parameters.AddWithValue("@mailingStreet2", viewModel.MailingAddress.Street2);
                            cmd.Parameters.AddWithValue("@MailingCity", viewModel.MailingAddress.City);
                            cmd.Parameters.AddWithValue("@mailingStateId", mailingAddressStateId);
                            cmd.Parameters.AddWithValue("@mailingZipCode", viewModel.MailingAddress.ZipCode);
                            cmd.Parameters.AddWithValue("@homePhone", viewModel.HomePhone);
                            cmd.Parameters.AddWithValue("@mobilePhone", viewModel.MobilePhone);
                            cmd.Parameters.AddWithValue("@emailAddress", viewModel.EmailAddress);
                            cmd.Parameters.AddWithValue("@userId", viewModel.UserId);
                            cmd.Parameters.AddWithValue("@profileId", viewModel.ProfileId);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    dbConnection.Close();
                }

                return this.Hydrate(viewModel.UserId);
            }

            return result;
        }

        private int GetMailingAddressStateId(UpdatableProfileInfoViewModel viewModel, DBConnection dbConnection)
        {
            var query = @"SELECT s.StateId
                            FROM bankexample.States s
                           WHERE s.Code = @stateCode";

            var cmd = new MySqlCommand();
            cmd.Connection = dbConnection.Connection;
            cmd.CommandText = query;
            cmd.Prepare();

            cmd.Parameters.AddWithValue("@stateCode", viewModel.MailingAddress.State);

            var mailingStateId = 0;

            var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                mailingStateId = reader.GetInt32("StateId");
            }
            else
            {
                query = @"SELECT s.StateId
                            FROM bankexample.States s
                           WHERE s.Code = @stateCode";

                cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@stateCode", "PA");

                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    mailingStateId = reader.GetInt32("StateId");
                }

                reader.Close();
            }

            reader.Close();

            return mailingStateId;
        }
    }
}
