﻿// <copyright file="SQLServerProfiles.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Providers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data;
    using Helpers;
    using Interfaces;
    using ViewModels;

    public class SQLServerProfiles : IProfiles
    {
        public ProfileInfoViewModel Hydrate(int userId)
        {
            var viewModel = new ProfileInfoViewModel();

            using (var entities = new BankExampleEntities())
            {
                var profile = entities.Profiles.FirstOrDefault(x => x.UserId == userId);
                if (profile != null)
                {
                    viewModel.SecurityToken = Sha256Hash.Sha256(profile.User.Username + profile.User.PasswordHash + Constants.ApplicationSecurityToken);
                    viewModel.ProfileId = profile.ProfileId;
                    viewModel.UserId = profile.UserId;
                    viewModel.Username = profile.User.Username;
                    viewModel.FirstName = profile.User.FirstName;
                    viewModel.LastName = profile.User.LastName;
                    viewModel.MailingAddress = new MailingAddressViewModel
                    {
                        Street1 = profile.MailingStreet1,
                        Street2 = profile.MailingStreet2,
                        City = profile.MailingCity,
                        StateId = profile.MailingStateId,
                        State = profile.State.Code,
                        ZipCode = profile.MailingZipCode
                    };

                    viewModel.HomePhone = profile.HomePhone;
                    viewModel.MobilePhone = profile.MobilePhone;
                    viewModel.EmailAddress = profile.EmailAddress;

                    viewModel.MailingAddress.StatesList = new List<StateViewModel>();

                    viewModel.MailingAddress.StatesList = (from s in entities.States
                                                           select new StateViewModel
                                                           {
                                                               StateId = s.StateId,
                                                               Name = s.Name,
                                                               Code = s.Code
                                                           }).ToList();
                }

                return viewModel;
            }
        }

        public ProfileInfoViewModel Dehydrate(UpdatableProfileInfoViewModel viewModel)
        {
            var result = new ProfileInfoViewModel();

            result.ValidationMessages = ProfileValidation.Validate(viewModel);

            if (!result.ValidationMessages.Any())
            {
                using (var entities = new BankExampleEntities())
                {
                    var profile = entities.Profiles.FirstOrDefault(x => x.UserId == viewModel.UserId && x.ProfileId == viewModel.ProfileId);
                    if (profile != null)
                    {
                        profile.MailingStreet1 = viewModel.MailingAddress.Street1;
                        profile.MailingStreet2 = viewModel.MailingAddress.Street2;
                        profile.MailingCity = viewModel.MailingAddress.City;

                        var state = entities.States.FirstOrDefault(x => x.Code == viewModel.MailingAddress.State);
                        if (state == null)
                        {
                            state = entities.States.FirstOrDefault(x => x.Code == "PA");
                        }

                        profile.State = state;
                        profile.MailingZipCode = viewModel.MailingAddress.ZipCode;

                        profile.HomePhone = viewModel.HomePhone;
                        profile.MobilePhone = viewModel.MobilePhone;
                        profile.EmailAddress = viewModel.EmailAddress;

                        profile.UpdatedById = profile.UserId;
                        profile.UpdatedDate = DateTime.Now;

                        entities.SaveChanges();

                        return this.Hydrate(viewModel.UserId);
                    }
                }
            }

            return result;
        }
    }
}
