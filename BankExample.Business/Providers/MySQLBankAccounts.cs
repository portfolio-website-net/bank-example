﻿// <copyright file="MySQLBankAccounts.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Providers
{
    using System;
    using System.Collections.Generic;
    using Data.MySQL;
    using Interfaces;
    using MySql.Data.MySqlClient;
    using ViewModels;

    public class MySQLBankAccounts : IBankAccounts
    {
        public List<BankAccountViewModel> Hydrate(int userId)
        {
            var viewModel = new List<BankAccountViewModel>();

            var dbConnection = DBConnection.Instance();
            if (dbConnection.IsConnect())
            {
                string query = @"SELECT a.AccountId, 
                                        a.AccountTypeId,
                                        at.Name AS AccountTypeName,
                                        at.Code AS AccountTypeCode,
                                        a.UserId,
                                        u.Username,
                                        a.Description,
                                        a.Balance,
                                        a.LastActivityDate,
                                        a.PaymentDueDate,
                                        a.MinimumAmountDue,
                                        a.InterestRate,
                                        a.MaturityDate
                                   FROM bankexample.Accounts a
                                  INNER JOIN bankexample.AccountTypes at ON a.AccountTypeId = at.AccountTypeId
                                  INNER JOIN bankexample.Users u ON a.UserId = u.UserId
                                  WHERE u.UserId = @userId";

                var cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@userId", userId);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int paymentDueDateIndex = reader.GetOrdinal("PaymentDueDate");
                    int minimumAmountDueIndex = reader.GetOrdinal("MinimumAmountDue");
                    int interestRateIndex = reader.GetOrdinal("InterestRate");
                    int maturityDateIndex = reader.GetOrdinal("MaturityDate");

                    viewModel.Add(new BankAccountViewModel()
                    {
                        AccountId = reader.GetInt32("AccountId"),
                        AccountTypeId = reader.GetInt32("AccountTypeId"),
                        AccountTypeName = reader.GetString("AccountTypeName"),
                        AccountTypeCode = reader.GetString("AccountTypeCode"),
                        UserId = reader.GetInt32("UserId"),
                        Username = reader.GetString("Username"),
                        Description = reader.GetString("Description"),
                        Balance = reader.GetDecimal("Balance"),
                        LastActivityDate = reader.GetDateTime("LastActivityDate"),
                        PaymentDueDate = reader.IsDBNull(paymentDueDateIndex) ? (DateTime?)null : reader.GetDateTime("PaymentDueDate"),
                        MinimumAmountDue = reader.IsDBNull(minimumAmountDueIndex) ? (decimal?)null : reader.GetDecimal("MinimumAmountDue"),
                        InterestRate = reader.IsDBNull(interestRateIndex) ? (double?)null : reader.GetDouble("InterestRate"),
                        MaturityDate = reader.IsDBNull(maturityDateIndex) ? (DateTime?)null : reader.GetDateTime("MaturityDate")
                    });
                }

                reader.Close();

                dbConnection.Close();
            }

            return viewModel;
        }
    }
}
