﻿// <copyright file="MySQLSecurity.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Providers
{
    using System.Collections.Generic;
    using Data.MySQL;
    using Helpers;
    using Interfaces;
    using MySql.Data.MySqlClient;
    using ViewModels;

    public class MySQLSecurity : ISecurity
    {
        public SecurityTokenViewModel GetSecurityToken(string username, string password)
        {
            var viewModel = new SecurityTokenViewModel();

            var dbConnection = DBConnection.Instance();
            if (dbConnection.IsConnect())
            {
                string query = @"SELECT u.UserId
                                   FROM bankexample.Users u
                                  WHERE u.Username = @username
                                    AND u.PasswordHash = @passwordHash";

                var cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                var passwordHash = Sha256Hash.Sha256(username + password + Constants.ApplicationSecurityToken);

                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@passwordHash", passwordHash);

                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    viewModel.SecurityToken = Sha256Hash.Sha256(username + passwordHash + Constants.ApplicationSecurityToken);
                    viewModel.UserId = reader.GetInt32("UserId");
                }

                dbConnection.Close();
            }

            return viewModel;
        }

        public bool ValidateUserLogin(int userId, string securityToken)
        {
            bool result = false;

            var dbConnection = DBConnection.Instance();
            if (dbConnection.IsConnect())
            {
                string query = @"SELECT u.Username,
                                        u.PasswordHash
                                   FROM bankexample.Users u
                                  WHERE u.UserId = @userId";

                var cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@userId", userId);

                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    var computedSecurityToken = Sha256Hash.Sha256(reader.GetString("Username") + reader.GetString("PasswordHash") + Constants.ApplicationSecurityToken);
                    if (securityToken == computedSecurityToken)
                    {
                        result = true;
                    }
                }

                dbConnection.Close();
            }

            return result;
        }

        public void InitializeUserPasswordHashes()
        {
            var dbConnection = DBConnection.Instance();
            if (dbConnection.IsConnect())
            {
                string query = @"SELECT u.UserId,
                                        u.Username
                                   FROM bankexample.Users u
                                  WHERE u.PasswordHash = ''";

                var cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                var users = new List<UserInfo>();

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new UserInfo
                    {
                        UserId = reader.GetInt32("UserId"),
                        Username = reader.GetString("Username")
                    });
                }

                reader.Close();

                foreach (var user in users)
                {
                    query = @"UPDATE bankexample.Users
                                 SET PasswordHash = @passwordHash,
                                     UpdatedById = @userId,
                                     UpdatedDate = NOW()
                               WHERE UserId = @userId";

                    cmd = new MySqlCommand();
                    cmd.Connection = dbConnection.Connection;
                    cmd.CommandText = query;
                    cmd.Prepare();

                    var initialPassword = user.Username + "#1";
                    var passwordHash = Sha256Hash.Sha256(user.Username + initialPassword + Constants.ApplicationSecurityToken);

                    cmd.Parameters.AddWithValue("@passwordHash", passwordHash);
                    cmd.Parameters.AddWithValue("@userId", user.UserId);

                    cmd.ExecuteNonQuery();
                }

                dbConnection.Close();
            }
        }

        private class UserInfo
        {
            public int UserId { get; set; }

            public string Username { get; set; }
        }
    }
}
