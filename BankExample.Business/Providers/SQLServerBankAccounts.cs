﻿// <copyright file="SQLServerBankAccounts.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Providers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data;
    using Helpers;
    using Interfaces;
    using ViewModels;

    public class SQLServerBankAccounts : IBankAccounts
    {
        public List<BankAccountViewModel> Hydrate(int userId)
        {
            var viewModel = new List<BankAccountViewModel>();

            using (var entities = new BankExampleEntities())
            {
                var accounts = entities.Accounts.Where(x => x.UserId == userId);
                if (accounts.Any())
                {
                    foreach (var account in accounts)
                    {
                        viewModel.Add(new BankAccountViewModel()
                        {
                            AccountId = account.AccountId,
                            AccountTypeId = account.AccountTypeId,
                            AccountTypeName = account.AccountType.Name,
                            AccountTypeCode = account.AccountType.Code,
                            UserId = account.UserId,
                            Username = account.User.Username,
                            Description = account.Description,
                            Balance = account.Balance,
                            LastActivityDate = account.LastActivityDate,
                            PaymentDueDate = account.PaymentDueDate,
                            MinimumAmountDue = account.MinimumAmountDue,
                            InterestRate = account.InterestRate,
                            MaturityDate = account.MaturityDate
                        });
                    }
                }

                return viewModel;
            }
        }
    }
}
