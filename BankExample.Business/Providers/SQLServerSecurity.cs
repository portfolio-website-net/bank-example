﻿// <copyright file="SQLServerSecurity.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Providers
{
    using System;
    using System.Linq;
    using Data;
    using Helpers;
    using Interfaces;
    using ViewModels;

    public class SQLServerSecurity : ISecurity
    {
        public SecurityTokenViewModel GetSecurityToken(string username, string password)
        {
            var viewModel = new SecurityTokenViewModel();
            using (var entities = new BankExampleEntities())
            {
                var passwordHash = Sha256Hash.Sha256(username + password + Constants.ApplicationSecurityToken);
                var user = entities.Users.FirstOrDefault(x => x.Username == username && x.PasswordHash == passwordHash);
                if (user != null)
                {
                    viewModel.SecurityToken = Sha256Hash.Sha256(username + passwordHash + Constants.ApplicationSecurityToken);
                    viewModel.UserId = user.UserId;
                }

                return viewModel;
            }
        }

        public bool ValidateUserLogin(int userId, string securityToken)
        {
            bool result = false;
            using (var entities = new BankExampleEntities())
            {
                var user = entities.Users.FirstOrDefault(x => x.UserId == userId);
                if (user != null)
                {
                    var computedSecurityToken = Sha256Hash.Sha256(user.Username + user.PasswordHash + Constants.ApplicationSecurityToken);
                    if (securityToken == computedSecurityToken)
                    {
                        result = true;
                    }
                }

                return result;
            }
        }

        public void InitializeUserPasswordHashes()
        {
            using (var entities = new BankExampleEntities())
            {
                var users = entities.Users.Where(x => x.PasswordHash == string.Empty).ToList();
                foreach (var user in users)
                {
                    var initialPassword = user.Username + "#1";
                    user.PasswordHash = Sha256Hash.Sha256(user.Username + initialPassword + Constants.ApplicationSecurityToken);

                    user.UpdatedById = user.UserId;
                    user.UpdatedDate = DateTime.Now;

                    entities.SaveChanges();
                }
            }
        }
    }
}
