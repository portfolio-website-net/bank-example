﻿// <copyright file="Sha256Hash.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Helpers
{
    using System.Security.Cryptography;
    using System.Text;

    public class Sha256Hash
    {
        // Code Source URL: https://stackoverflow.com/questions/12416249/hashing-a-string-with-sha256
        public static string Sha256(string value)
        {
            var crypt = new SHA256Managed();
            var hash = new StringBuilder();
            var crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(value));
            foreach (var theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }

            return hash.ToString();
        }
    }
}
