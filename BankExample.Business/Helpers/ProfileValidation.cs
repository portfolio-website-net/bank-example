﻿// <copyright file="ProfileValidation.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using ViewModels;

    public class ProfileValidation
    {
        public static List<string> Validate(UpdatableProfileInfoViewModel viewModel)
        {
            var messages = new List<string>();

            if (string.IsNullOrEmpty(viewModel.MailingAddress.Street1))
            {
                messages.Add("Street 1 is required.");
            }

            if (string.IsNullOrEmpty(viewModel.MailingAddress.City))
            {
                messages.Add("City is required.");
            }

            if (string.IsNullOrEmpty(viewModel.MailingAddress.State))
            {
                messages.Add("State is required.");
            }

            if (string.IsNullOrEmpty(viewModel.MailingAddress.ZipCode))
            {
                messages.Add("Zip Code is required.");
            }
            else
            {
                if (viewModel.MailingAddress.ZipCode.EndsWith("-"))
                {
                    viewModel.MailingAddress.ZipCode = viewModel.MailingAddress.ZipCode.Substring(0, viewModel.MailingAddress.ZipCode.Length - 1);
                }

                // Reference URL: https://stackoverflow.com/questions/14942602/c-validation-for-us-or-canadian-zip-code#14942826
                var usZipRegEx = @"^\d{5}(?:[-\s]\d{4})?$";

                if (!Regex.Match(viewModel.MailingAddress.ZipCode, usZipRegEx).Success)
                {
                    messages.Add("Zip Code must be in the format '12345' or '12345-1234'.");
                }
            }

            if (string.IsNullOrEmpty(viewModel.HomePhone)
                && string.IsNullOrEmpty(viewModel.MobilePhone))
            {
                messages.Add("At least one phone number is required.");
            }
            else
            {
                // Reference URL: https://stackoverflow.com/questions/18091324/regex-to-match-all-us-phone-number-formats#18091377
                var phoneRegEx = @"^\(?\d{3}\)?-? *\d{3}-? *-?\d{4}$";

                if (!string.IsNullOrEmpty(viewModel.HomePhone)
                    && !Regex.Match(viewModel.HomePhone, phoneRegEx).Success)
                {
                    messages.Add("Home Phone must be in the format '123-123-1234'.");
                }

                if (!string.IsNullOrEmpty(viewModel.MobilePhone)
                    && !Regex.Match(viewModel.MobilePhone, phoneRegEx).Success)
                {
                    messages.Add("Mobile Phone must be in the format '123-123-1234'.");
                }
            }

            if (string.IsNullOrEmpty(viewModel.EmailAddress))
            {
                messages.Add("Email Address is required.");
            }
            else
            {
                // Reference URL: https://stackoverflow.com/questions/16167983/best-regular-expression-for-email-validation-in-c-sharp
                var emailRegEx = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

                if (!Regex.Match(viewModel.EmailAddress, emailRegEx, RegexOptions.IgnoreCase).Success)
                {
                    messages.Add("Email Address must be in the format 'user@domain.com'.");
                }
            }

            return messages;
        }
    }
}
