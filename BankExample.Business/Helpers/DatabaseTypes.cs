﻿// <copyright file="DatabaseTypes.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace BankExample.Business.Helpers
{
    public enum DatabaseTypes
    {
        SQLServer,
        MySQL
    }
}
