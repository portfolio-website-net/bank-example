﻿CREATE TABLE `BankExample`.Profiles (
    `ProfileId`      INT            AUTO_INCREMENT  NOT NULL,
    `UserId`         INT            NOT NULL,
    `MailingStreet1` NVARCHAR (100) NOT NULL,
    `MailingStreet2` NVARCHAR (100) NULL,
    `MailingCity`    NVARCHAR (100) NOT NULL,
    `MailingStateId` INT            NOT NULL,
    `MailingZipCode` NVARCHAR (10)  NOT NULL,
    `HomePhone`      NVARCHAR (15)  NULL,
    `MobilePhone`    NVARCHAR (15)  NULL,
    `EmailAddress`   NVARCHAR (100) NOT NULL,
    `CreatedById`    INT            NOT NULL,
    `CreatedDate`    DATETIME(3)       NOT NULL,
    `UpdatedById`    INT            NULL,
    `UpdatedDate`    DATETIME(3)       NULL,
    CONSTRAINT `PK_Profiles` PRIMARY KEY (`ProfileId` ASC),
    CONSTRAINT `FK_Profiles_States` FOREIGN KEY (`MailingStateId`) REFERENCES `BankExample`.States (`StateId`),
    CONSTRAINT `FK_Profiles_Users` FOREIGN KEY (`UserId`) REFERENCES `BankExample`.Users (`UserId`)
);

