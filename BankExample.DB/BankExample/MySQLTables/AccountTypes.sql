﻿CREATE TABLE `BankExample`.AccountTypes (
    `AccountTypeId` INT           AUTO_INCREMENT  NOT NULL,
    `Name`          NVARCHAR (50) NOT NULL,
    `Code`          NVARCHAR (20) NOT NULL,
    `CreatedById`   INT           NOT NULL,
    `CreatedDate`   DATETIME(3)      NOT NULL,
    `UpdatedById`   INT           NULL,
    `UpdatedDate`   DATETIME(3)      NULL,
    CONSTRAINT `PK_AccountTypes` PRIMARY KEY (`AccountTypeId` ASC)
);

