﻿CREATE TABLE `BankExample`.Accounts (
    `AccountId`        INT             AUTO_INCREMENT  NOT NULL,
    `AccountTypeId`    INT             NOT NULL,
    `UserId`           INT             NOT NULL,
    `Description`      NVARCHAR (100)  NOT NULL,
    `Balance`          DECIMAL (19, 2) NOT NULL,
    `LastActivityDate` DATETIME(3)        NOT NULL,
    `PaymentDueDate`   DATE            NULL,
    `MinimumAmountDue` DECIMAL (19, 2) NULL,
    `InterestRate`     DOUBLE       NULL,
    `MaturityDate`     DATE            NULL,
    `CreatedById`      INT             NOT NULL,
    `CreatedDate`      DATETIME(3)        NOT NULL,
    `UpdatedById`      INT             NULL,
    `UpdatedDate`      DATETIME(3)        NULL,
    CONSTRAINT `PK_Accounts` PRIMARY KEY (`AccountId` ASC),
    CONSTRAINT `FK_Accounts_AccountTypes` FOREIGN KEY (`AccountTypeId`) REFERENCES `BankExample`.AccountTypes (`AccountTypeId`),
    CONSTRAINT `FK_Accounts_Users` FOREIGN KEY (`UserId`) REFERENCES `BankExample`.Users (`UserId`)
);

