﻿CREATE TABLE `BankExample`.Users (
    `UserId`       INT            AUTO_INCREMENT  NOT NULL,
    `Username`     NVARCHAR (50)  NULL,
    `FirstName`    NVARCHAR (100) NULL,
    `LastName`     NVARCHAR (100) NULL,
    `PasswordHash` NVARCHAR (100) NOT NULL,
    `CreatedById`  INT            NOT NULL,
    `CreatedDate`  DATETIME(3)       NOT NULL,
    `UpdatedById`  INT            NULL,
    `UpdatedDate`  DATETIME(3)       NULL,
    CONSTRAINT `PK_Users` PRIMARY KEY (`UserId` ASC)
);

