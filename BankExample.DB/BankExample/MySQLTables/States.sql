﻿CREATE TABLE `BankExample`.States (
    `StateId`     INT           AUTO_INCREMENT  NOT NULL,
    `Code`        NVARCHAR (2)  NULL,
    `Name`        NVARCHAR (50) NULL,
    `CreatedById` INT           NOT NULL,
    `CreatedDate` DATETIME(3)      NOT NULL,
    `UpdatedById` INT           NULL,
    `UpdatedDate` DATETIME(3)      NULL,
    CONSTRAINT `PK_States` PRIMARY KEY (`StateId` ASC)
);

