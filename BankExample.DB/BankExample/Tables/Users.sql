﻿CREATE TABLE [BankExample].[Users] (
    [UserId]       INT            IDENTITY (1, 1) NOT NULL,
    [Username]     NVARCHAR (50)  NULL,
    [FirstName]    NVARCHAR (100) NULL,
    [LastName]     NVARCHAR (100) NULL,
    [PasswordHash] NVARCHAR (100) NOT NULL,
    [CreatedById]  INT            NOT NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [UpdatedById]  INT            NULL,
    [UpdatedDate]  DATETIME       NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserId] ASC)
);
GO

