﻿CREATE TABLE [BankExample].[AccountTypes] (
    [AccountTypeId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (50) NOT NULL,
    [Code]          NVARCHAR (20) NOT NULL,
    [CreatedById]   INT           NOT NULL,
    [CreatedDate]   DATETIME      NOT NULL,
    [UpdatedById]   INT           NULL,
    [UpdatedDate]   DATETIME      NULL,
    CONSTRAINT [PK_AccountTypes] PRIMARY KEY CLUSTERED ([AccountTypeId] ASC)
);
GO

