﻿CREATE TABLE [BankExample].[Accounts] (
    [AccountId]        INT             IDENTITY (1, 1) NOT NULL,
    [AccountTypeId]    INT             NOT NULL,
    [UserId]           INT             NOT NULL,
    [Description]      NVARCHAR (100)  NOT NULL,
    [Balance]          DECIMAL (19, 2) NOT NULL,
    [LastActivityDate] DATETIME        NOT NULL,
    [PaymentDueDate]   DATE            NULL,
    [MinimumAmountDue] DECIMAL (19, 2) NULL,
    [InterestRate]     FLOAT (53)      NULL,
    [MaturityDate]     DATE            NULL,
    [CreatedById]      INT             NOT NULL,
    [CreatedDate]      DATETIME        NOT NULL,
    [UpdatedById]      INT             NULL,
    [UpdatedDate]      DATETIME        NULL,
    CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED ([AccountId] ASC),
    CONSTRAINT [FK_Accounts_AccountTypes] FOREIGN KEY ([AccountTypeId]) REFERENCES [BankExample].[AccountTypes] ([AccountTypeId]),
    CONSTRAINT [FK_Accounts_Users] FOREIGN KEY ([UserId]) REFERENCES [BankExample].[Users] ([UserId])
);
GO

