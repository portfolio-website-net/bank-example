﻿CREATE TABLE [BankExample].[States] (
    [StateId]     INT           IDENTITY (1, 1) NOT NULL,
    [Code]        NVARCHAR (2)  NULL,
    [Name]        NVARCHAR (50) NULL,
    [CreatedById] INT           NOT NULL,
    [CreatedDate] DATETIME      NOT NULL,
    [UpdatedById] INT           NULL,
    [UpdatedDate] DATETIME      NULL,
    CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED ([StateId] ASC)
);
GO

