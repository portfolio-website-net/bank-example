# Vue.js with a REST API

> This examples uses a front-end web application accessing a REST API to view a user's Savings, Checking, Certificate of Deposit, and Loan information.  The implementation also allows for profile viewing and updates.

## :computer: Technologies

- **REST API:** ASP.NET Web API (.NET 4.6.1) using [TinyIoC](https://github.com/grumpydev/TinyIoC) running on IIS
- **Front-end Web Application:** ASP.NET MVC (.NET 4.6.1) with [Vue.js](https://vuejs.org) 2.X, [VueMaskPlugin](https://github.com/probil/v-mask), and [Bootstrap 4.1](https://getbootstrap.com) running on IIS
- **Databases Supported:** Microsoft SQL Server 2017+ (via Entity Framework) and MySQL 5.6.15+ (via MySql.Data)

## :cd: Setup

1. Clone this repository to your local machine.
2. In your local repository, open **BankExample.sln** in Visual Studio 2015 (or above) running as an administrator (to automatically create the virtual directories in IIS).
   - Visual Studio should automatically create a virtual directory for **BankExample.API** and **BankExample.UI**.  If it doesn't, go to Properties->Web for each project and set the Local IIS Project Urls to http://localhost/BankExample.API and http://localhost/BankExample.UI, respectively.
5. Install [Microsoft SQL Server 2017](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) and Microsoft SQL Server Management Studio (if not already installed).
6. Install MySQL (if not already installed).  Use [EasyPHP DevServer](https://bitbucket.org/easyphp/easyphp-devserver/downloads/EasyPHP-DevServer-14.1VC9-install.exe) and [MySQL Workbench](https://www.mysql.com/products/workbench/) to get the database set up quickly.
7. [Run this script to initialize SQL Server.](https://gitlab.com/portfolio-website-net/bank-example/blob/master/DatabaseScripts/Initialize%20SQL%20Server.sql) This script will:
   - Create a new database in SQL Server called **BankExample**.
   - Add a new user/password **BankExample/BankExample#1** with access to the **BankExample** database.
   - Create the tables and populate the example data.
8. [Run this script to initialize MySQL.](https://gitlab.com/portfolio-website-net/bank-example/blob/master/DatabaseScripts/Initialize%20MySQL.sql) This script will:
   - Create a new schema in MySQL called **bankexample** and create the tables and populate the example data.
   - *Note: If using EasyPHP DevServer, the localhost root user will already be set up.  Use [MySQL Workbench](https://www.mysql.com/products/workbench/) to connect and set up the database.*

## :building_construction: Build

Run **Build Solution** in Visual Studio.  Assuming the virtual directories and databases are configured as described above, all dependencies are now accounted for.

## :rocket: Run

1. Set **BankExample.UI** as the StartUp Project.
2. Click **Play** in Visual Studio or open the URL in your web browser: [http://localhost/BankExample.UI](http://localhost/BankExample.UI).
3. This example has its own front-end, but you are welcome to run the curl commands below to try out the API.
4. To log in, use the following pre-populated user credentials:

Username | Password
------------ | -------------
bsmith | bsmith#1
hwilson | hwilson#1
mjackson | mjackson#1

## :gear: Configuration Updates
1. To change the target database type, update the following application setting in the BankExample.API Web.config:
```xml
Use SQL Server: <add key="DatabaseType" value="SQLServer" />
Use MySQL: <add key="DatabaseType" value="MySQL" />
```
2. To adjust the connection strings, update the following in the BankExample.API Web.config:
```xml
<add name="BankExampleEntities" connectionString="metadata=res://*/DataEntities.csdl|res://*/DataEntities.ssdl|res://*/DataEntities.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=localhost;initial catalog=BankExample;User Id=BankExample;Password=BankExample#1;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />
<add name="MySQLDefaultConnection" connectionString="Server=localhost; database=bankexample; UID=root; password=; SslMode=None;" />
```
3. To update the REST API endpoint URL, update the following application setting in the BankExample.UI Web.config:
```xml
<add key="BankAPIUrl" value="http://localhost/BankExample.API"></add>
```

## :white_square_button: Example curl Commands

Log in as **bsmith**:
```
curl -X GET --header "username: bsmith" --header "password: bsmith#1" http://localhost/BankExample.API/api/Security
```
Log in as **hwilson**:
```
curl -X GET --header "username: hwilson" --header "password: hwilson#1" http://localhost/BankExample.API/api/Security
```
Log in as **mjackson**:
```
curl -X GET --header "username: mjackson" --header "password: mjackson#1" http://localhost/BankExample.API/api/Security
```
Example of an invalid login:
```
curl -X GET --header "username: bsmith" --header "password: bsmith#" http://localhost/BankExample.API/api/Security
```
Example of getting a user's profile with the user's **SecurityToken** and **UserId**:
```
curl -X GET --header "securityToken: ea6b074f3f547b15895edd6daccf14f57a179b44b233f8e976f093587a9aa5fd" http://localhost/BankExample.API/api/Profiles/1
```
Example of getting a user's bank accounts with the user's **SecurityToken** and **UserId**:
```
curl -X GET --header "securityToken: ea6b074f3f547b15895edd6daccf14f57a179b44b233f8e976f093587a9aa5fd" http://localhost/BankExample.API/api/BankAccounts/1
```
Example of updating a user's profile with the user's **SecurityToken** and encoded profile data:
```
curl -X POST --header "securityToken: ea6b074f3f547b15895edd6daccf14f57a179b44b233f8e976f093587a9aa5fd" --data-ascii "ProfileId=1&UserId=1&Username=bsmith&FirstName=Bill&LastName=Smith&FullName=Bill+Smith&MailingAddress"%"5BStreet1"%"5D=200+Main+St.&MailingAddress"%"5BStreet2"%"5D=Apt+"%"235&MailingAddress"%"5BCity"%"5D=Harrisburg&MailingAddress"%"5BStateId"%"5D=389&MailingAddress"%"5BState"%"5D=PA&MailingAddress"%"5BZipCode"%"5D=17100-&HomePhone=&MobilePhone=123-124-1455&EmailAddress=bsmith12"%"40gmail.com" http://localhost/BankExample.API/api/Profiles
```

